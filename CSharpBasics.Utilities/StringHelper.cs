﻿using System;

namespace CSharpBasics.Utilities
{
	public class StringHelper
	{
		/// <summary>
		/// Вычисляет среднюю длину слова в переданной строке без учётов знаков препинания и специальных символов
		/// </summary>
		/// <param name="inputString">Исходная строка</param>
		/// <returns>Средняя длина слова</returns>
		public int GetAverageWordLength(string inputString)
		{
			if (inputString == null) return 0;
			string[] words = inputString.Split(' ');
			int wordsNum = 0;
			int sum = 0;
			foreach (string word in words)
			{
				int counter = 0;
				bool isWord = false;
				for (int i = 0; i < word.Length; i++)
				{
					if (char.IsLetter(word[i]))
					{
						isWord = true;
						counter++;
					}
				}
				if (isWord) wordsNum++;
				sum += counter;
			}
			if (sum == 0 || wordsNum == 0)	return 0;
			return sum / wordsNum;
		}

		/// <summary>
		/// Удваивает в строке <see cref="original"/> все буквы, принадлежащие строке <see cref="toDuplicate"/>
		/// </summary>
		/// <param name="original">Строка, символы в которой нужно удвоить</param>
		/// <param name="toDuplicate">Строка, символы из которой нужно удвоить</param>
		/// <returns>Строка <see cref="original"/> с удвоенными символами, которые есть в строке <see cref="toDuplicate"/></returns>
		public string DuplicateCharsInString(string original, string toDuplicate)
		{
			if (original == null) return null;
			if (toDuplicate == null) return original;
			System.Text.StringBuilder sb = new System.Text.StringBuilder();

			for (int i = 0; i < original.Length; i++)
			{
				sb.Append(original[i]);
				foreach (char c in toDuplicate)
				{
					if (original[i].ToString().Equals(c.ToString(), StringComparison.OrdinalIgnoreCase))
					{
						sb.Append(original[i]);
						break;
					}					
				}
			}
			return sb.ToString();
		}
	}
}