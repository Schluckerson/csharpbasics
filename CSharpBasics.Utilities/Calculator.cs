﻿using System;

namespace CSharpBasics.Utilities
{
	public class Calculator
	{
		/// <summary>
		/// Вычисляет сумму всех натуральных чисел меньше <see cref="number"/>, кратных любому из <see cref="divisors"/>
		/// </summary>
		/// <param name="number">Натуральное число</param>
		/// <param name="divisors">Числа, которым должны быть кратны слагаемые искомой суммы</param>
		/// <returns>Вычисленная сумма</returns>
		/// <exception cref="ArgumentOutOfRangeException">
		/// Выбрасывается в случае, если <see cref="number"/> или любое из чисел в <see cref="divisors"/> не является натуральным,
		/// а также если массив <see cref="divisors"/> пустой
		/// </exception>
		public float CalcSumOfDivisors(int number, params int[] divisors)
		{

			if (divisors.Length == 0) throw new ArgumentOutOfRangeException("Массив делителей пустой.");
			if (number <= 0) throw new ArgumentOutOfRangeException("Число меньше или равно нулю.");
			int sum = 0;
			for (int i = 1; i < number; i++)
			{
				foreach (int divisor in divisors)
				{
					if (divisor <= 0) throw new ArgumentOutOfRangeException("Один из делителей не является натуральным.");
					if (i % divisor == 0) sum += i;
				}
			}
			return sum;
		}

		/// <summary>
		/// Возвращает ближайшее наибольшее целое, состоящее из цифр исходного числа <see cref="number"/>
		/// </summary>
		/// <param name="number">Исходное число</param>
		/// <returns>Ближайшее наибольшее целое</returns>
		public long FindNextBiggerNumber(int number)
		{
			int biggerNum = 0;
			if (number < 1) throw new ArgumentOutOfRangeException("Число меньше единицы.");

			int[] nums = Array.ConvertAll(number.ToString().ToCharArray(), c => (int)Char.GetNumericValue(c));
			int temp;

			int changePoint = -1;
			for (int i = nums.Length - 1; i > 0; i--)
			{
				if (nums[i] > nums[i - 1])
				{
					changePoint = i - 1;
					break;
				}
			}

			if (changePoint == -1) throw new System.InvalidOperationException();

			int smallest = changePoint + 1;
			for (int i = nums.Length - 1; i > changePoint; i--)
			{
				if (nums[changePoint] < nums[i] && nums[i] < nums[smallest]) smallest = i;
			}

			temp = nums[changePoint];
			nums[changePoint] = nums[smallest];
			nums[smallest] = temp;
			Array.Sort(nums, changePoint + 1, nums.Length - (changePoint + 1));
			int len = nums.Length - 1;
			for (int j = 0; j < nums.Length; j++)
			{
				biggerNum += nums[j] * (int)Math.Pow(10, len);
				len -= 1;
			}

			return biggerNum;
		}
	}
}